/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardDiscount', [ //
    'ngMaterialDashboard',//
]);

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardDiscount')
/**
 * 
 */
.config(function($routeProvider) {
	$routeProvider//
	.when('/discounts', {
		controller : 'AmdDiscountsCtrl',
		templateUrl : 'views/amd-discount-discounts.html',
		name : 'Discounts',
		icon : 'money_off',
		groups: ['discount-management'],
		navigate : true,
		protect: true,
	})//
	.when('/discounts/new', {
		controller : 'AmdDiscountNewCtrl',
		templateUrl : 'views/amd-discount-new.html',
		name : 'New Discount',
		icon: 'card_giftcard',
		groups: ['discount-management'],
		navigate : true,
		protect: true,
	})//
	.when('/discount/:discountId', {
		controller : 'AmdDiscountCtrl',
		templateUrl : 'views/amd-discount-discount.html',
		protect: true,
	});
});

'use strict';

angular.module('ngMaterialDashboardDiscount')

/**
 * @ngdoc controller
 * @name AmdDiscountNewCtrl
 * @description Manages a new discount view
 */
.controller('AmdDiscountNewCtrl', function($scope, /*$discount, */$resource, $navigator) {

	var ctrl = {
			savingDiscount : false
	};

	function cancel() {
		$navigator.openPage('discounts');
	}

	function add(config) {
		ctrl.savingDiscount = true;
		var data = config.model;
		$discount.newDiscount(data)//
		.then(function(obj) {
			ctrl.savingDiscount = false;
			$navigator.openPage('discounts');
		}, function(error) {
			ctrl.savingDiscount = false;
			var message = 'Fail to create new discount.';
			if(error.data){
				message = error.data.message;
			}
			alert('Fail to create discount:' + error.data.message);
		});
	}

	/**
	 * Load banks
	 * 
	 * @returns
	 */
	function loadDiscountTypes()
	{
		return $discount.discountTypes()//
		.then(function(dTypes){
			$scope.discountTypes = dTypes;
		});
	}

	$scope.selectUser = function(){
		return $resource.get('userId')//
		.then(function(userId){
			$scope.config.model.user = userId;
		})
	};

	$scope.cancel = cancel;
	$scope.add = add;
	$scope.loadDiscountTypes = loadDiscountTypes;
	$scope.ctrl = ctrl;
});

'use strict';

angular.module('ngMaterialDashboardDiscount')

/**
 * @ngdoc function
 * @name ngMaterialDashboardDiscount.controller:DiscountCtrl
 * @description # DiscountCtrl Controller of the ngMaterialDashboardDiscount
 */
.controller('AmdDiscountCtrl', function($scope, /*$discount,*/ $navigator, $routeParams, $location) {

	var ctrl = {
			loadingDiscount : true,
			savingDiscount : false,
			items: [],
			edit: false
	};
	var discount;


	function handlError(){
		alert('faile to load discount');
	}

	/**
	 * درخواست مورد نظر را از سیستم حذف می‌کند.
	 * 
	 * @param request
	 * @returns
	 */
	function remove() {
		confirm('delete discount ' + $scope.discount.id +'?')//
		.then(function(){
			return $scope.discount.delete();//
		})//
		.then(function(){
			// TODO: maso, 1395: go to the model page
			$location.path('discounts');
		}, function(error){
			alert('fail to delete discount:' + error.message);
		});
	}

	function save(){
		ctrl.savingDiscount = true;
		discount.update()//
		.then(function(){
			ctrl.edit=false;
			ctrl.savingDiscount = false;
		}, function(){
			alert('An error is occured while updating discount.');
			ctrl.savingDiscount = false;			
		});
	}

	/*
	 * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری شده است.
	 */
	$scope.remove = remove;
	$scope.save = save;

	$scope.ctrl = ctrl;
//
//	// Load discount
//	$discount.discount($routeParams.discountId)//
//	.then(function(a){
//		discount = a;
//		$scope.discount = a;
//		ctrl.loadingDiscount = false;
//		return a;
//	}, handlError);
});


'use strict';

angular.module('ngMaterialDashboardDiscount')

/**
 * @ngdoc controller
 * @name AmdDiscountsCtrl
 * @description Manages list of all discounts
 * 
 * 
 */
.controller('AmdDiscountsCtrl', function($scope, /*$discount, */$navigator, QueryParameter) {

	var paginatorParameter = new QueryParameter();
	paginatorParameter.setOrder('id', 'd');
	var requests = null;
	var ctrl = {
			loadDiscounts : false,
			status: 'relax',
			items: []
	};

	/**
	 * Search for discounts
	 * 
	 * @NOTE This function is added to $scope
	 * 
	 * @memberof AmdDiscountsCtrl
	 * @param query
	 * @returns promiss
	 */
	function find(query) {
		paginatorParameter.setQuery(query);
		paginatorParameter.setPage(1);
		return reload();
	}

	/**
	 * لود کردن داده‌های صفحه بعد
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.loadDiscounts || ctrl.status === 'working') {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}
		ctrl.status = 'working';
		ctrl.loadDiscounts = true;
		$discount.discounts(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
			ctrl.status = 'relax';
			ctrl.loadDiscounts = false;
		}, function() {
			ctrl.status = 'fail';
			ctrl.loadDiscounts = false;
		});
	}


	function addDiscount(){
		$navigator.openPage('discounts/new');
	}

	/**
	 * تمام حالت‌های کنترلر را دوباره مقدار دهی می‌کند.
	 * 
	 * @returns
	 */
	function reload(){
		requests = null;
		ctrl.items = [];
		paginatorParameter.setPage(1);
		nextPage();
	}

	/**
	 * دسته مورد نظر را از سیستم حذف می‌کند.
	 * 
	 * @param SdpDiscount
	 * @returns
	 */
	function remove(object) {
		return object.delete()//
		.then(function(){
			var index = ctrl.items.indexOf(object);
			if (index > -1) {
				ctrl.items.splice(index, 1);
			}
		});
	}

	$scope.items = [];
	$scope.reload = reload;
	$scope.search = find;
	$scope.nextPage = nextPage;
	$scope.remove = remove;
	$scope.add = addDiscount;
	$scope.ctrl = ctrl;

	$scope.paginatorParameter = paginatorParameter;
	$scope.sortKeys = [ 'id',//
		'code',//
		'type',//
		'count',//
		'remain_count', //
		'off_value', 
		'valid_day', //
		'user', //
		'name', //
		'creation_dtime' ];

	$scope.moreActions = [ {
		title : 'New discount',
		icon : 'add',
		action : $scope.add
	} ];

});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardDiscount')
/**
 * دریچه‌های محاوره‌ای
 */
.run(function($navigator) {
	$navigator
	.newGroup({
		id: 'discount-management',
		title: 'Discount management',
		description: 'Manage defined discount in the system.',
		icon: 'money_off',
		hidden: '!app.user.tenant_owner',
		priority: 5
	});
});
angular.module('ngMaterialDashboardDiscount').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amd-discount-discount.html',
    "<md-content class=md-padding layout-padding flex> <section ng-show=!ctrl.edit mb-preloading=ctrl.loadingDiscount layout=column md-whiteframe=1&quot; layout-margin> <table> <tr> <td>ID </td> <td>: {{discount.id}}</td> </tr> <tr> <td>Code </td> <td>: {{discount.code}}</td> </tr> <tr> <td>Type </td> <td>: {{discount.type}}</td> </tr> <tr> <td>Off Value </td> <td>: {{discount.off_value}}</td> </tr> <tr> <td>Count </td> <td>: {{discount.count}}</td> </tr> <tr> <td>Remain Count </td> <td>: {{discount.remain_count}}</td> </tr> <tr> <td>Valid Day </td> <td>: {{discount.valid_day}}</td> </tr> <tr> <td>User</td> <td>: {{discount.user}}</td> </tr> <tr> <td>Name </td> <td>: {{discount.name}}</td> </tr> <tr> <td>Description </td> <td>: {{discount.description}}</td> </tr> <tr> <td>Creation Date</td> <td>: {{discount.creation_dtime}}</td> </tr> </table>  <div ng-show=!ctrl.edit layout=row> <span flex></span> <md-button class=\"md-raised md-primary\" ng-click=remove()> <wb-icon>delete</wb-icon> <span translate>Delete</span> </md-button> <md-button class=md-raised ng-click=\"ctrl.edit=true\"> <wb-icon>edit</wb-icon> <span translate>Edit</span> </md-button> </div> </section> <section ng-show=ctrl.edit mb-preloading=ctrl.savingDiscount layout=column md-whiteframe=1 layout-margin> <md-input-container class=md-block flex-gt-xs> <label>Off Value</label> <input type=number ng-model=discount.off_value> </md-input-container> <md-input-container class=md-block flex-gt-xs> <label>Valid Day</label> <input type=number ng-model=discount.valid_day> </md-input-container> <md-input-container class=md-block flex-gt-xs> <label>Name</label> <input ng-model=discount.name> </md-input-container> <md-input-container class=md-block flex-gt-xs> <label>Description</label> <input ng-model=discount.description> </md-input-container> <md-input-container class=md-block flex-gt-xs> <label>Title</label> <input ng-model=discount.title> </md-input-container> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=save()> <wb-icon>save</wb-icon> <span translate>Save</span> </md-button> <md-button class=md-raised ng-click=\"ctrl.edit=false\"> <wb-icon>close</wb-icon> <span translate>Cancel</span> </md-button> </div> </section> </md-content>"
  );


  $templateCache.put('views/amd-discount-discounts.html',
    " <md-content wb-infinate-scroll=nextPage layout=column flex>  <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload mb-sort-keys=sortKeys mb-more-actions=moreActions> </mb-pagination-bar>  <md-list> <md-subheader class=md-no-sticky>Discounts</md-subheader> <md-list-item ng-repeat=\"pobject in ctrl.items track by pobject.id\" class=md-3-line ng-href=\"{{'discount/'+pobject.id}}\"> <wb-icon>card_giftcard</wb-icon> <div class=md-list-item-text layout=column> <h3>{{pobject.id}} - {{pobject.code}}</h3> <h4>Off: {{pobject.off_value}}, Remain count: {{pobject.remain_count ? pobject.remain_count : 0}} of {{pobject.count}}, Valid day: {{pobject.valid_day ? pobject.valid_day : 'undefined'}} </h4> <p><b>{{pobject.name}}</b>, {{pobject.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item>  <div layout=column layout-align=\"center center\"> <md-progress-circular ng-show=\"ctrl.status === 'working'\" md-diameter=96> Loading ... </md-progress-circular> </div> </md-list> <div layout=column layout-align=\"center center\" ng-show=\"!ctrl.items || ctrl.items.length == 0\"> <h2>Empty discount list</h2> <p>No discount match with the query. You can add a new discount.</p> <md-button ng-click=add()> <wb-icon>add</wb-icon> <span translate>Add</span> </md-button> </div> </md-content>"
  );


  $templateCache.put('views/amd-discount-new.html',
    "<md-content class=md-padding layout-padding flex> <form name=discountForm ng-action=add(config) mb-preloading=ctrl.savingDiscount layout-margin layout=column flex> <md-input-container> <label translate=\"\">Code</label> <input name=code ng-model=config.model.code required> <div ng-messages=discountForm.code.$error> <div ng-message=required>This is required!</div> </div> </md-input-container> <md-input-container class=md-block flex-gt-sm> <label translate=\"\">Type</label> <md-select required placeholder=Type md-on-open=loadDiscountTypes() ng-model=config.model.type> <md-option ng-repeat=\"dtype in discountTypes.items\" value={{dtype.type}}> {{dtype.title}} </md-option> </md-select> </md-input-container> <md-input-container> <label translate=\"\">Count</label> <input type=number ng-model=config.model.count> </md-input-container> <md-input-container> <label translate=\"\">Off value</label> <input type=number ng-model=config.model.off_value> </md-input-container> <div layout-gt-sm=row> <md-input-container class=md-block flex-gt-sm> <label translate=\"\">User</label> <input ng-model=config.model.user> </md-input-container> <md-button ng-click=selectUser()>select</md-button> </div> <md-input-container> <label translate=\"\">Valid day</label> <input type=number ng-model=config.model.valid_day> </md-input-container> <md-input-container> <label translate=\"\">Name</label> <input ng-model=config.model.name> </md-input-container> <md-input-container> <label translate=\"\">Description</label> <input ng-model=config.model.description> </md-input-container> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=add(config)> <wb-icon aria-label=close>done</wb-icon> <span translate=\"\">Add</span> </md-button> <md-button class=md-raised ng-click=cancel()> <wb-icon aria-label=close>close</wb-icon> <span translate=\"\">Cancel</span> </md-button> </div> </form> </md-content>"
  );

}]);
