/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardDiscount')
/**
 * 
 */
.config(function($routeProvider) {
	$routeProvider//
	.when('/discounts', {
		controller : 'AmdDiscountsCtrl',
		templateUrl : 'views/amd-discount-discounts.html',
		name : 'Discounts',
		icon : 'money_off',
		groups: ['discount-management'],
		navigate : true,
		protect: true,
	})//
	.when('/discounts/new', {
		controller : 'AmdDiscountNewCtrl',
		templateUrl : 'views/amd-discount-new.html',
		name : 'New Discount',
		icon: 'card_giftcard',
		groups: ['discount-management'],
		navigate : true,
		protect: true,
	})//
	.when('/discount/:discountId', {
		controller : 'AmdDiscountCtrl',
		templateUrl : 'views/amd-discount-discount.html',
		protect: true,
	});
});
