'use strict';

describe('Controller AmdDiscount', function() {

	// load the controller's module
	beforeEach(module('ngMaterialDashboardDiscount'));

	var AmdDiscountCtrl;
	var scope;

	// Initialize the controller and a mock scope
	beforeEach(inject(function($controller, $rootScope, _$usr_) {
		scope = $rootScope.$new();
		AmdDiscountCtrl = $controller('AmdDiscountCtrl', {
			$scope : scope,
			$usr : _$usr_
			// place here mocked dependencies
		});
	}));

	it('should be defined', function() {
		expect(angular.isDefined(AmdDiscountCtrl)).toBe(true);
	});
});
